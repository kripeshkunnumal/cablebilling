/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.cablebilling.servic;
import in.ac.gpckasaragod.cablebilling.ui.PackageDetailForm;
import in.ac.gpskasaragod.cablebilling.ui.model.data.PackageDetail;
import java.util.List;

/**
 *
 * @author student
 */
public interface PackageDetailService {
   public String savePackageDetail(Integer Id, String packageInfo, Double Price,String packageDescription);
   public PackageDetail readPackageDetail(Integer id);
    public String updatePacakageDetail(Integer id,String packageInfo,Double Price,String packageDescription);
    public String deletePcakgeDetail(Integer id);
  public List<PackageDetail> getAllPackageDetail();    

    public PackageDetailForm readPackageDetailForm(Integer id);

    public String deletePackageDetails(Integer id);
}
