/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.cablebilling.service.Impl;

import in.ac.gpckasaragod.cablebilling.servic.PackageDetailService;
import in.ac.gpckasaragod.cablebilling.ui.PackageDetailForm;
import in.ac.gpskasaragod.cablebilling.ui.model.data.PackageDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */

    public class PackageDetailServiceImpl extends ConnectionServiceImpl implements PackageDetailService{

    private String packagedescription;
    private String packageinfo;
    private String price;
    public String savePackageDetail(Integer Id, String Packageinfo, Float Price,String Packagedescription) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO PACKAGEDETAILS (PACKAGE,PRICE,PACKAGEDESCRIPTION) VALUES "
                +"( '"+packageinfo+"','"+price+"','"+packagedescription+"')"; 
                                System.err.println("Query:"+query);
            
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(PackageDetailServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
}
    @Override
    public PackageDetail readPackageDetail(Integer Id){
        PackageDetail packageDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM PACKAGEDETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String packageInfo = resultSet.getString("PACKAGE");
                Double Price = resultSet.getDouble("PRICE");
                String packageDescription = resultSet.getString("PACKAGEDESCRIPTION");
                packageDetails = new PackageDetail(id,packageInfo,Price,packageDescription);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(PackageDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return packageDetails;
       
    }
    @Override
      public List<PackageDetail> getAllPackageDetail(){
        List<PackageDetail> packagedetails = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT PACKAGEDAETAILS.ID,PACKAGE,PRICE,PACKAGEDESCRIPTION";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             Integer id = resultSet.getInt("ID");
                String packageuInfo = resultSet.getString("PACKAGE");
                Double Price = resultSet.getDouble("PRICE");
                String packageDescription = resultSet.getString("PACKAGEDESCRIPTION");
                PackageDetails packageDetails = new PackageDetails(id,packageinfo,packagedescription);
                PackageDetails.add(packageDetails);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(PackageDetailServiceImpl.class.getName()).log(Level.SEVERE,
                    null, ex);
    }
        return packagedetails;
    
    }
     
     public String updatePacakageDetail(Integer id,String packageinfo,Float price,String packagedescription){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE PCKAGEDETAILS SET PACKAGE='"+packageinfo+"',PRICE="+price+"'WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(SQLException ex){
            return "Update failed";
        }
    }
    @Override
     public String deletePcakgeDetail(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM PCAKGEDETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           ex.printStackTrace();
           return "Delete failed";
        }
     
}

    @Override
    public String savePackageDetail(Integer Id, String packageInfo, Double Price, String packageDescription) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String updatePacakageDetail(Integer id, String packageinfo, Double price, String packageDescription) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public PackageDetailForm readPackageDetailForm(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String deletePackageDetails(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}


    

