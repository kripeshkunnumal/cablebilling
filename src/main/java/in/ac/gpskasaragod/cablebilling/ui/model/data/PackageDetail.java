/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpskasaragod.cablebilling.ui.model.data;

/**
 *
 * @author student
 */
public class PackageDetail {
    private Integer id;
    private  String packageinfo;
    private  Float price;
    private  String packagedescription;

    public PackageDetail(Integer id, String packageinfo, Float price, String packagedescription) {
        this.id = id;
        this.packageinfo = packageinfo;
        this.price = price;
        this.packagedescription = packagedescription;
    }

    public PackageDetail(Integer id, String packageinfo, Double Price, String packageDescription) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPackageinfo() {
        return packageinfo;
    }

    public void setPackageinfo(String packageinfo) {
        this.packageinfo = packageinfo;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getPackagedescription() {
        return packagedescription;
    }

    public void setPackagedescription(String packagedescription) {
        this.packagedescription = packagedescription;
    }
    
    
}
